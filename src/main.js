import Vue from 'vue';
import App from './App.vue';

import ElementUI from 'element-ui';
import { ElementTiptapPlugin } from 'element-tiptap';
import 'element-tiptap/lib/index.css';

Vue.use(ElementUI);
Vue.use(ElementTiptapPlugin, {
   lang: document.getElementById('curlocale').attributes.lang.value,
  // spellcheck: false,
});

Vue.config.productionTip = false;




// new Vue({
//   render: h => h(App)
// }).$mount('#app');





// create a constructor for your widget
var Widget = Vue.extend({
  render(h) {
    return h(App, {
      props: {
        query:      this.$el.attributes.wid
      }
    })
  },
 // .. other options, but not `el:`
})

var nodes = document.querySelectorAll('.elements-tiptap-root')
for (var i = 0; i < nodes.length; ++i) {
  new Widget({el: nodes[i] })
}